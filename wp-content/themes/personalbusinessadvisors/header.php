<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="icon" type="image/ico" href="<?php the_field("favicon", 'option');?>">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:400,600,600i,700,700i" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/mediaquery.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery.mmenu.all.css">
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/css3-mediaqueries.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
<![endif]-->



<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="wrapper">
<header>
	<div class="header_top">
		<div class="container">
			<div class="mob-nav">
                        <a class="menu-btn" href="#menu"></a>
                       
                    </div>
			<div class="logo">
			<a href="<?php echo site_url(); ?>"><img src="<?php the_field("logo", 'option');?>" alt=""></a>
		</div>
		 <nav id="menu">
                        
                          <?php wp_nav_menu( array('menu' => 'Header Menu')); ?>
                        
                    </nav>

</div>
	</div>
<div class="header_bottom">
	<div class="container">
		
		
				<div class="desk-nav">

<?php wp_nav_menu( array('menu' => 'Header Menu')); ?>

</div>
		
	</div>
</div>

</header>
<?php if(!is_front_page()) { ?>

<?php inner_banner() ?>

<div class="breadcrumbs" typeof="BreadcrumbList">
	<div class="container">
<?php if(function_exists('bcn_display'))
{
bcn_display();
}?>
</div>
</div>
<?php } ?>