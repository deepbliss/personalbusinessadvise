<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		

		<footer id="colophon" class="site-footer" role="contentinfo">
      <div class="footer_inner">
      <div class="container">
        <div class="footer_top">
          <div class="clmn_1 column">
        <h4>ABOUT PBA</h4>
        <?php wp_nav_menu( array('menu' => 'About PBA Menu')); ?>
      </div>
      <div class="clmn_2 column">
        <h4>THE JOB MARKET</h4>
        <?php wp_nav_menu( array('menu' => 'The Job Market Menu')); ?>
      </div>
      <div class="clmn_3 column">
        <h4>OWNERSHIP OPPORTUNITIES</h4>
        <?php wp_nav_menu( array('menu' => 'Ownership Opportunites Menu')); ?>
      </div>
       <div class="clmn_4 column">
        <h4>INHOUSE <br>OPPORTUNITIES</h4>
        <?php wp_nav_menu( array('menu' => 'Inhouse Opportinities Menu')); ?>
      </div>
       <div class="clmn_5 column">
        <h4>INTERNATIONAL OPPORTUNITIES</h4>
        <?php wp_nav_menu( array('menu' => 'International Opportunities Menu')); ?>
      </div>
       <div class="clmn_6 column">
        <h4>In The News</h4>
        <?php wp_nav_menu( array('menu' => 'In The News Menu')); ?>
      </div>
        </div>
        <div class="footer_btm">
          <div class="bbb_logo">
          <a href="https://www.bbb.org/us/tx/helotes/profile/personnel-consultants/personal-business-advisors-0825-90014678#sealclick" target="_blank"><img src="<?php the_field("bbb_logo", 'option');?>" alt=""></a>
        </div>
        <div class="social_sec">
           <ul>
    <?php 
          if( have_rows("social_icon", 'option') ): 
            while ( have_rows("social_icon", 'option') ) : the_row();?>
            <li>
             <a href="<?php the_sub_field("social_link"); ?>" target="_blank" ><img src="<?php the_sub_field("social_image"); ?>"></a>
            </li>
          <?php endwhile; 
        endif; ?>
      </ul>
        </div>
        </div>
        <div class="copyright">
          <p>Copyright &copy; 2002 – <script type="text/javascript">var d = new Date();document.write(d.getFullYear());</script> By Personal Business Advisors ® All Rights Reserved.    |    Website design by <a target="_blank" href="http://profitgateweb.com/">Profit Gate, Inc.</a></p>
        </div>
      </div>
      </div>
		
		</footer><!-- #colophon -->
	</div><!-- .site-content-contain -->

<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.3.min.js"></script> 

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mmenu.min.all.js"></script>

<script>
$(function() {
$('nav#menu').mmenu({
"extensions": [
                  "fx-menu-zoom",
                  "pagedim-black"
               ],
           
            navbars:[ {
                position: 'top',
                content: ['prev', 'title', 'close']
            }
           
            ]
               

});
});
</script>

<script>
$(document).ready(function() {
$('#banner').owlCarousel({
loop: true,
items: 1,
autoplay: true,
smartSpeed: 3000,
rewindNav:false,
nav: false,
margin:10,
dots: false

});
$('#testimonial').owlCarousel({
loop: true,
items: 1,
autoplay: true,
smartSpeed: 1500,
rewindNav:false,
nav: false,
margin:10,
dots: true

});
});
</script>
 <script>
$window = $(window);
$window.scroll(function() {
  $scroll_position = $window.scrollTop();
    if ($scroll_position > 1) { // if body is scrolled down by 300 pixels
        $('header').addClass('sticky');

        // to get rid of jerk
        header_height = $('.header').innerHeight();
        $('body').css('padding-top' , header_height);
    } else {
        $('body').css('padding-top' , '0');
        $('header').removeClass('sticky');
    }
 });
</script>
<script type="text/javascript">
  $(document).ready(function() {
$(".left_sec .heading.mobile").click(function(){
$(this).toggleClass('open');
$(".left_sec ul").fadeToggle();
});
});
</script>
<script type="text/javascript">
document.addEventListener( 'wpcf7mailsent', function( event ) {
       location = 'https://thejock.net/Development/Personalbusinessadvisors/thank-you';
}, false );
</script>
</body>
</html>
