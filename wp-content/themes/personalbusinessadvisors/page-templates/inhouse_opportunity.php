<?php
/**
 * Template Name: Inhouse Opportunity Page
 *
 **/
get_header(); ?>
<div class="inner_page">
	<div class="container">
<div class="left_sec">
	<div class="top_left">
		<div class="heading desktop">
			<span><?php the_title(); ?></span>
		</div>
		<div class="heading mobile">
			<span><?php the_title(); ?></span>
		</div>
		 <?php wp_nav_menu( array('menu' => 'Inhouse Opportinities Menu')); ?>
	</div>
	
</div>
<div class="rgt_sec">
	<h1><?php the_title(); ?></h1>
	<?php if(get_field('video_url') || get_field('video_content')){ ?>

	<div class="inner_video_sec">
		<div class="left_video_sec">
			
			<?php the_field('video_content'); ?>
		
		</div>
		<?php if(get_field('video_url')){ ?>
		<div class="rgt_video_sec">
			<div class="video">
			<?php the_field('video_url'); ?>
			</div>
		</div>
		<?php } ?>
	</div><?php } ?>
	<div class="btm_cnt">
		<?php while ( have_posts() ) : the_post(); 
the_content(); 
endwhile; ?>
	</div>
</div>
<div class="btm_left">
	<div class="heading">
			<span>Testimonials</span>
		</div>
		<?php testimonial() ?>
	</div>
</div>

</div>
<?php get_footer(); ?>