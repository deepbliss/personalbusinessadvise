<?php
/**
 * Template Name: Machine page
 *
 **/
get_header();
global $product; ?>
<div class="inner_page">
<div class="container">
<div class="landing_page">
<h1><?php the_title(); ?></h1>
<?php  $args = array( 'post_type' => 'product',  'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'ID');
$the_query = new WP_Query($args);
$thumb = 1;
 if($the_query -> have_posts())
 { 
  ?>
<table>
	<thead>
	<tr>
		<th class="pro_name">EXCAVATOR/SPEC</th>
		<th class="pro_attachment">ATTACHMENTS</th>
		<th class="pro_rate">RATES</th>
		<th class="pro_img"></th>
	</tr>
	</thead>
	<tbody>

<?php  while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
<tr>	
<td class="pro_name"><h3><?php the_title(); ?></h3>
<div class="desc">
<?php 
the_content(); 
 ?>
		</div>
		</td>
		<td class="pro_attachment">

 <ul>
		<?php 
					if( have_rows('attachments') ): 
						while ( have_rows('attachments') ) : the_row();?>
						<li>
							<?php the_sub_field("attachment" , $post->ID); ?>
									
								
							
						</li>
					<?php endwhile; 
				endif; ?>
			</ul>
			<div class="call_nw">
			<a href="tel:<?php the_field('phone_number'); ?>">Call Now</a>
			</div>

</td>
		<td class="pro_rate">

 <ul>
		<?php 
					if( have_rows('rates') ): 
						while ( have_rows('rates') ) : the_row();?>
						<li>
							<?php the_sub_field("rate" , $post->ID); ?>
									
								
							
						</li>
					<?php endwhile; 
				endif; ?>
			</ul>
			<div class="call_nw">
			<a href="<?php the_field('enquiry_link'); ?>">Enquire Online</a>
			</div>

</td>
<td class="pro_img">
<?php $fullimg = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_id()), 'full', true, '' ); ?>
<figure>
<span>
<?php
$attachment_ids = $product->get_gallery_image_ids();
if ( $attachment_ids && has_post_thumbnail() ) { ?>

 <img src="<?php echo $fullimg[0]; ?>" alt="" title="" />


<?php }	

else
{ ?> 
<a class="fancybox-thumb" data-fancybox-group="thumb-<?php echo $thumb; ?>" href="<?php echo $fullimg[0]; ?>" title="">
<img src="<?php echo $fullimg[0]; ?>" alt="" title="" />
</a>
<?php } ?>

</figure>
</span>

<?php
$attachment_ids = $product->get_gallery_image_ids();
if ( $attachment_ids && has_post_thumbnail() ) { ?>
<div class="call_nw">
<a class="fancybox-thumb" data-fancybox-group="thumb-<?php echo $thumb; ?>" href="<?php echo $fullimg[0]; ?>" title="">More Images</a>
</div>
	<div class="more-image">
	<?php foreach ( $attachment_ids as $attachment_id ) {
	
		?> <a class="fancybox-thumb" data-fancybox-group="thumb-<?php echo $thumb; ?>" href="<?php echo wp_get_attachment_url( $attachment_id ); ?>" title=""><img src="<?php echo wp_get_attachment_url( $attachment_id ); ?>" alt="" title="" /></a>
<?php
	}
	?>
	</div>
<?php } ?>
</td>


</tr>
<?php $thumb++; endwhile; ?>

	</tbody>
</table>
<?php }  ?>
</div>
</div>
</div>
<?php get_footer(); ?>