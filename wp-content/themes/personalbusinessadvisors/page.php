<?php

get_header(); ?>
<div class="inner_page">
	<div class="container">
		<div class="btm_cnt">
			<?php while ( have_posts() ) : the_post(); 
	the_content(); 
	endwhile; ?>
		</div>
	</div>	
</div>
<?php get_footer(); ?>