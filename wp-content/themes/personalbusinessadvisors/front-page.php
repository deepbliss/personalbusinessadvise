<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<div class="home_inner">
 <div class="homepage-banner">
  <?php home_banner() ?>
</div>
<div class="banner_btm_sec">
	<div class="container">
		<h2><?php the_field("banner_bottom_text"); ?></h2>
	</div>
</div>
<div class="video_sec">
	<div class="container">
		<div class="video_lft">
			<div class="video">
				<?php the_field("video"); ?>
			</div>
		</div>
		<div class="video_rgt">
			<h3><?php the_field("video_heading"); ?></h3>
			<div class="video_txt">
				<?php the_field("video_text"); ?>
			</div>
			<a href="<?php the_field("video_link"); ?>">+ Read more</a>
		</div>
	</div>
	</div>
	<div class="home_middle_sec">
		<div class="home_middle_top_sec">
			<div class="container">
				<div class="middle_left">
					<h3><?php the_field("home_middle_section_title"); ?></h3>
					<h5><?php the_field("home_middle_section_sub_title"); ?></h5>
					
						<?php the_field("home_middle_section_content"); ?>
					
				</div>
				<div class="middle_rgt">
					<figure>
						<img src="<?php the_field("home_middle_section_image"); ?>" alt="<?php the_field("home_middle_section_title"); ?>">
					
					</figure>
				</div>
			</div>
		</div>
		<div class="home_middle_bottom_sec">
			<div class="container">
				<div class="middle_text">
				<h4><?php the_field("home_middle_section_bottom_text"); ?></h4>
			</div>
			</div>
		</div>

	</div>
	<div class="testimonial_sec">
		<div class="container">
			<div class="title">
				<span>Testimonials</span>
			</div>
		 <?php testimonial() ?>
		</div>
	</div>
	<div class="home_btm_sec">
		<div class="container">
			<div class="home_btm_left">
				<h2><?php the_field("home_bottom_left_section_title"); ?></h2>
				<div class="content">
					<?php the_field("home_bottom_left_section_content"); ?>
				</div>
				<a href="<?php the_field("home_bottom_left_section_link"); ?>">+ Read more</a>
			</div>
			<div class="home_btm_rgt">
				<h2><?php the_field("home_bottom_right_section_title"); ?></h2>
				<figure>
					<span><img src="<?php echo get_template_directory_uri(); ?>/images/call_img.png"></span>
				</figure>
				<div class="content">
					<?php the_field("home_bottom_right_section_content"); ?>	
				</div>
				</div>
		</div>
	</div>
</div>
</div><!-- #primary -->

<?php get_footer();
